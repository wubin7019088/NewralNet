package newralnet.core;

import org.apache.commons.math3.linear.RealMatrix;
import org.apache.commons.math3.linear.RealVector;

import newralnet.net.ModularNet;
import newralnet.util.LayerGradient;

/** 
 *	An abstract class describing a layer of a {@link ModularNet} which contains trainable parameters (e.g. Weights and biases)
 */
public abstract class TrainableLayer extends Layer{
	/**
	 * {@link RealMatrix} containing the weights of the {@link TrainableLayer}
	 */
	public RealMatrix weights;
	/**
	 * {@link RealVector} containing the biases of the {@link TrainableLayer}
	 */
	public RealVector biases;
	/**
	 * Method which randomly initializes the parameters of the {@link TrainableLayer}. <br>
	 * It is called by the {@link ModularNet} if the <code>initNet</code> argument is <code>true</code>
	 */
	public abstract void initializeTrainableParameters();
	
	/**
	 * Calculates the gradients of the trainable parameters with respect to the error in the sucessive {@link Layer}
	 * @param errors - A {@link RealMatrix} containing the errors in the sucessive {@link Layer}
	 * @return A {@link LayerGradient} containing the gradients its trainable parameters 
	 */
	public abstract LayerGradient calcBPOutput(RealMatrix errors);
	
	
}