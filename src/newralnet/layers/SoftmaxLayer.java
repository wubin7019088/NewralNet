package newralnet.layers;

import org.apache.commons.math3.linear.Array2DRowRealMatrix;
import org.apache.commons.math3.linear.RealMatrix;

import newralnet.core.Layer;
import newralnet.util.NewralOperations;
import newralnet.util.NewralUtils;

/**
 * A {@link Layer} passing forward the result of the Softmax function
 */
public class SoftmaxLayer extends Layer{
	int inSize;
	@Override
	public void initialize(int inColumnDimension, int inRowDimention) {
		super.initialize(inColumnDimension, inRowDimention);
		inSize=inColumnDimension*inRowDimention;
	}
	
	@Override
	public RealMatrix passForward(RealMatrix in) {
		//e^(in)
		this.in=in;
		RealMatrix inputExp = NewralUtils.componentwiseOperation(NewralOperations.expOperation, in);
		double expSum = NewralUtils.elementSum(inputExp);
		out = inputExp.scalarMultiply(1/expSum);
		return out;
	}

	@Override
	public RealMatrix passBackward(RealMatrix in) {
		RealMatrix passBack=new Array2DRowRealMatrix(inRD, inCD);
		for(int r=0; r<inRD; r++){//Jacobi fun - Easier way?
			for(int c=0; c<inCD; c++){
				double sum=0;
				for(int rj=0; rj<inRD; rj++){
					for(int cj=0; cj<inCD; cj++){
						double der;
						if(r==rj && c==cj){
							der=out.getEntry(r, c)*(1-out.getEntry(rj, cj));
						}else{
							der=-out.getEntry(r, c)*out.getEntry(rj, cj);
						}
						sum+=der*in.getEntry(rj, cj);
					}
				}
				passBack.setEntry(r, c, sum);
			}
		}
		return passBack;
	}

	@Override
	public Layer copy() {
		SoftmaxLayer newLayer=new SoftmaxLayer();
		newLayer.initialize(inCD, inRD);
		return newLayer;
	}

	@Override
	public int getOutColumnDimension() {
		return inCD;
	}

	@Override
	public int getOutRowDimension() {
		return inRD;
	}
	
	@Override
	public String getSaveString() {
		return inCD+"&"+inRD;
	}

	@Override
	public void constructFromSaveString(String str) {
		String[] splited=str.split("&");
		initialize(Integer.parseInt(splited[0]), Integer.parseInt(splited[1]));
	}

}
