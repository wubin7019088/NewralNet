package newralnet.cost;

import org.apache.commons.math3.linear.RealMatrix;

import newralnet.core.Cost;
import newralnet.util.NewralOperations;
import newralnet.util.NewralUtils;

/**
 * Cross Entropy {@link Cost} function 
 */
public class CrossEntropyCost extends Cost{

	@Override
	public double cost(RealMatrix predicted, RealMatrix label) {
		return -1*NewralUtils.elementSum(
			NewralUtils.componentwiseOperation(NewralOperations.hadamardProductOperation, 
					label, 
					NewralUtils.componentwiseOperation(NewralOperations.LnOperation, predicted)
			)
			.add(
			NewralUtils.componentwiseOperation(NewralOperations.hadamardProductOperation,
				label.scalarMultiply(-1).scalarAdd(1),
				NewralUtils.componentwiseOperation(NewralOperations.LnOperation, predicted.scalarMultiply(-1).scalarAdd(1))
			)
			)
		);
	}

	@Override
	public RealMatrix costPrime(RealMatrix predicted, RealMatrix label) {
		return NewralUtils.componentwiseOperation(NewralOperations.hadamardDivisionOperation, 
			label.scalarMultiply(-1).scalarAdd(1),
			predicted.scalarMultiply(-1).scalarAdd(1)
		).subtract(
		NewralUtils.componentwiseOperation(NewralOperations.hadamardDivisionOperation,
			label,
			predicted
		)	
		);	
	}

}
